//FUNCTION CODING:

//Translate the other students specifics into their own respective objects. Make sure to include the login, logout, and listGrades method in each object, as well.

/*
Name: Joe
Email: joe@mail.com
Grades: 78, 82, 79, 85

Name: Jane
Email: jane@mail.com
Grades: 87, 89, 91, 93

Name: Jessie
Email: jessie@mail.com
Grades: 91, 89, 92, 93
*/

//Define a method for EACH student object that will compute for their grade average (total of grades divided by 4).

/* 


const studentOne = {
  Name: 'Joe',
  Email: 'joe@mail.com',
  Grades: [78, 82, 79, 85],

  calcAvgGrade() {
    const calc = this.Grades.reduce((prev, current) => prev + current);
    return calc / this.Grades.length;
  },

  willPass() {
    return this.calcAvgGrade() >= 85 ? true : false;
  },
};

const studentTwo = {
  Name: 'Jane',
  Email: 'jane@mail.com',
  Grades: [87, 89, 91, 93],

  calcAvgGrade() {
    const calc = this.Grades.reduce((prev, current) => prev + current);
    return calc / this.Grades.length;
  },

  willPass() {
    return this.calcAvgGrade() >= 85 ? true : false;
  },
};

const studentThree = {
  Name: 'Jessie',
  Email: 'jessie@mail.com',
  Grades: [91, 89, 92, 93],

  calcAvgGrade() {
    const calc = this.Grades.reduce((prev, current) => prev + current);
    return calc / this.Grades.length;
  },

};


*/

//Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

/* 

const studentOne = {
  Name: 'Joe',
  Email: 'joe@mail.com',
  Grades: [78, 82, 79, 85],

  calcAvgGrade() {
    const calc = this.Grades.reduce((prev, current) => prev + current);
    return calc / this.Grades.length;
  },

  willPass() {
    return this.calcAvgGrade() >= 85 ? true : false;
  },
};

const studentTwo = {
  Name: 'Jane',
  Email: 'jane@mail.com',
  Grades: [87, 89, 91, 93],

  calcAvgGrade() {
    const calc = this.Grades.reduce((prev, current) => prev + current);
    return calc / this.Grades.length;
  },

  willPass() {
    return this.calcAvgGrade() >= 85 ? true : false;
  },
};

const studentThree = {
  Name: 'Jessie',
  Email: 'jessie@mail.com',
  Grades: [91, 89, 92, 93],

  calcAvgGrade() {
    const calc = this.Grades.reduce((prev, current) => prev + current);
    return calc / this.Grades.length;
  },

  willPass() {
    return this.calcAvgGrade() >= 85 ? true : false;
  },
};


*/

//Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

const studentOne = {
  Name: 'Joe',
  Email: 'joe@mail.com',
  Grades: [78, 82, 79, 85],

  calcAvgGrade() {
    const calc = this.Grades.reduce((prev, current) => prev + current);
    return calc / this.Grades.length;
  },

  willPass() {
    return this.calcAvgGrade() >= 85 ? true : false;
  },

  willPassWithHonors() {
    if (this.calcAvgGrade() < 85) {
      return 'undefined';
    }

    if (this.calcAvgGrade() >= 90) {
      return true;
    } else if (this.calcAvgGrade() >= 85 && this.calcAvgGrade() < 90) {
      return false;
    }
  },
};

const studentTwo = {
  Name: 'Jane',
  Email: 'jane@mail.com',
  Grades: [87, 89, 91, 93],

  calcAvgGrade() {
    const calc = this.Grades.reduce((prev, current) => prev + current);
    return calc / this.Grades.length;
  },

  willPass() {
    return this.calcAvgGrade() >= 85 ? true : false;
  },

  //   willPassWithHonors() {
  //     return this.calcAvgGrade() >= 90
  //       ? true
  //       : this.calcAvgGrade() >= 85 && this.calcAvgGrade() < 90 && false;
  //   },

  willPassWithHonors() {
    if (this.calcAvgGrade() < 85) {
      return 'undefined';
    }

    if (this.calcAvgGrade() >= 90) {
      return true;
    } else if (this.calcAvgGrade() >= 85 && this.calcAvgGrade() < 90) {
      return false;
    }
  },
};

const studentThree = {
  Name: 'Jessie',
  Email: 'jessie@mail.com',
  Grades: [91, 89, 92, 93],

  calcAvgGrade() {
    const calc = this.Grades.reduce((prev, current) => prev + current);
    return calc / this.Grades.length;
  },

  willPass() {
    return this.calcAvgGrade() >= 85 ? true : false;
  },

  willPassWithHonors() {
    if (this.calcAvgGrade() < 85) {
      return 'undefined';
    }

    if (this.calcAvgGrade() >= 90) {
      return true;
    } else if (this.calcAvgGrade() >= 85 && this.calcAvgGrade() < 90) {
      return false;
    }
  },
};

//Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

/* const classOf1A = {
  studends: [studentOne, studentTwo, studentThree],
}; */

//Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

/* const classOf1A = {
  students: [studentOne, studentTwo, studentThree],

  countHonorStudents() {
    let counter = 0;

    this.students.forEach((el) => {
      el.willPassWithHonors() && counter++;
    });

    return counter;
  },
};

console.log(classOf1A.countHonorStudents()); */

//Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

/* const classOf1A = {
  students: [studentOne, studentTwo, studentThree],

  countHonorStudents() {
    let counter = 0;

    this.students.forEach((el) => {
      el.willPassWithHonors() && counter++;
    });

    return counter;
  },

  honorsPercentage() {
    const calc = Math.floor(100 / this.students.length);
    return `${calc * this.countHonorStudents()}%`;
  },
};

console.log(classOf1A.honorsPercentage()); */

//Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

/* const classOf1A = {
  students: [studentOne, studentTwo, studentThree],

  countHonorStudents() {
    let counter = 0;

    this.students.forEach((el) => {
      el.willPassWithHonors() && counter++;
    });

    return counter;
  },

  honorsPercentage() {
    const calc = Math.floor(100 / this.students.length);
    return `${calc * this.countHonorStudents()}%`;
  },

  retrieveHonorStudentInfo() {
    //emails avg grade
    const studentsData = [];

    this.students.forEach((el) => {
      el.willPassWithHonors() &&
        studentsData.push({
          email: el.Email,
          avgGrade: el.calcAvgGrade(),
        });
    });

    return studentsData;
  },
};

console.log(classOf1A.retrieveHonorStudentInfo()); */

//Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

const classOf1A = {
  students: [studentThree, studentOne, studentTwo],

  countHonorStudents() {
    let counter = 0;

    this.students.forEach((el) => {
      el.willPassWithHonors() && counter++;
    });

    return counter;
  },

  honorsPercentage() {
    const calc = Math.floor(100 / this.students.length);
    return `${calc * this.countHonorStudents()}%`;
  },

  retrieveHonorStudentInfo() {
    //emails avg grade
    const studentsData = [];

    this.students.forEach((el) => {
      el.willPassWithHonors() &&
        el.willPassWithHonors() !== 'undefined' &&
        studentsData.push({
          email: el.Email,
          avgGrade: el.calcAvgGrade(),
        });
    });

    return studentsData;
  },

  sortHonorStudentsByGradeDesc() {
    const test = this.retrieveHonorStudentInfo().sort((elA, elB) => {
      return elA.avgGrade - elB.avgGrade;
    });

    return test;
  },
};

console.log(classOf1A.retrieveHonorStudentInfo());
console.log(classOf1A.sortHonorStudentsByGradeDesc());
